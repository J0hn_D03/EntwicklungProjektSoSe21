package EntwicklungProjektSoSe21;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Entity
public class Sportler {
	private static final Logger log = LogManager.getLogger(Sportler.class);
	
	@Id
	private String name;
	private String geschlecht;
	private String gr��e;
	
	@OneToMany (mappedBy = "sportler", cascade=CascadeType.ALL)
	private List<TrainingSession> trainingSession;


	public Sportler() {
		super();
	}
	public Sportler (String name, String geschlecht, String gr��e) {
		super();
		this.setName(name);
		this.setGeschlecht(geschlecht);
		this.setGr��e(gr��e);
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGeschlecht() {
		return geschlecht;
	}
	public void setGeschlecht(String geschlecht) {
		this.geschlecht = geschlecht;
	}
	public String getGr��e() {
		return gr��e;
	}
	public void setGr��e(String gr��e) {
		this.gr��e = gr��e;
	}
	
	public List<TrainingSession> getTrainingSession() {
		log.info("getTrainingSession wurde aufgerufen");
		return trainingSession;
	}
	public void setTrainingSession(List<TrainingSession> trainingSession) {
				log.info("setTrainingSession wurde aufgerufen");
				for (TrainingSession session : trainingSession) {
					session.setSportler(this);
				}
		
		this.trainingSession = trainingSession;
	}
	
	public void addTrainingSession(TrainingSession trainingSession) {
		log.info("addTrainingSession wurde aufgerufen");
		this.trainingSession.add(trainingSession);
	}
}
