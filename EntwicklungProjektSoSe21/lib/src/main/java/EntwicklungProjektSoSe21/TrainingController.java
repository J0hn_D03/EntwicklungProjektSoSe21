package EntwicklungProjektSoSe21;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;


@RestController
public class TrainingController {
		private static final Logger log = LogManager.getLogger(TrainingController.class);
		@Autowired
		TrainingService trainingService;
	
		/**
		 * Anzeigen aller Trainings
		 * @apiNote "/training"
		 * @return Liste von Trainings
		 */
		
		@RequestMapping("/training")
		public List<Training> getTrainingList(){
			log.info("/training API Call (get)");
			return trainingService.getTrainingList();
			
		}
		
		/**
		 * Anzeigen eines Trainings
		 * @apiNote "/training/{tname}"
		 * @param
		 * @return Training
		 */
		
		@RequestMapping("/training/{tname}")
		public Training getTraining(@PathVariable String tname){
			log.info("/training/{tname} API Call (get)");
			return trainingService.getTraining(tname);
		}
		
		/**
		 * Setzen eines Trainings
		 * @apiNote "/training"
		 * @param
		 */
		
		@RequestMapping(method=RequestMethod.POST, value="/training")
		public void addTraining(@RequestBody Training training) {
			log.info("/training API Call (post)");
			trainingService.addTraining(training);
		}
		
		/**
		 * Aktualisieren eines Trainings
		 * @apiNote "/training/{tname}"
		 * @param
		 */
		
		@RequestMapping(method=RequestMethod.PUT, value="/training/{tname}")
		public void updateTraining(@PathVariable String tname,@RequestBody Training training) {
			log.info("/training/{tname} API Call (post)");
			trainingService.updateTraining(tname, training);
		}
		
		/**
		 * L�schen eines Trainings
		 * @apiNote "/training/{tname}"
		 * @param
		 */
		
		@RequestMapping(method=RequestMethod.DELETE, value="/training/{tname}")
		public void deleteTraining(@PathVariable String tname) {
			log.info("/training/{tname} API Call (Delete)");
			trainingService.deleteTraining(tname);
		}
		
	
}
