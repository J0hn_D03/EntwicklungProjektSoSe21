package EntwicklungProjektSoSe21;

import org.springframework.data.repository.CrudRepository;

public interface SportlerRepository extends CrudRepository<Sportler, String>{

}
