package EntwicklungProjektSoSe21;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SportlerService {
	
	@Autowired
	private SportlerRepository sportlerRepository;
	
	
	public List<Sportler> getSportlerList(){
		
		Iterator<Sportler>it = sportlerRepository.findAll().iterator();
		ArrayList<Sportler> myList = new ArrayList<>();
		while (it.hasNext()) 
			myList.add(it.next());	
		
		
		return myList;
		
	}
	
	
	public Sportler getSportler(String name) {
        return sportlerRepository.findById(name).orElse(null);
	}


	public void addSportler(Sportler sportler) {
		sportlerRepository.save(sportler);
		
	}
	
	public void updateSportler(Sportler sportler, String name) {
		sportlerRepository.deleteById(name);
		sportlerRepository.save(sportler);
		
	}

	public void deleteSportler(String name) {
		sportlerRepository.deleteById(name);
		
	}





}
