package EntwicklungProjektSoSe21;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Entity
public class Training {
	private static final Logger log = LogManager.getLogger(Training.class);
	@Id
	private String tname;
	
	private String beschreibung;
	
	@OneToMany(mappedBy="training")
	private List<TrainingSession> TrainingSession;

	public Training() {}
	public Training(String tname, String beschreibung) {
		super();
		this.tname = tname;
		this.beschreibung = beschreibung;
		log.info("Training wurde angleget");
	}
	
	//getter & setter
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public String getBeschreibung() {
		return beschreibung;
	}
	public void setBeschreibung(String bezeichnung) {
		this.beschreibung = bezeichnung;
	}


}
