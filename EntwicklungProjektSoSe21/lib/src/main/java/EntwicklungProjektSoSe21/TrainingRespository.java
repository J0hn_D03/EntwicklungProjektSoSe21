package EntwicklungProjektSoSe21;


import org.springframework.data.repository.CrudRepository;

public interface TrainingRespository extends CrudRepository<Training, String>{
	
}

