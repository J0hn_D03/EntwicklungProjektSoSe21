package EntwicklungProjektSoSe21;



import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SportlerController {
	
	private static final Logger log = LogManager.getLogger(SportlerController.class);
	@Autowired 
	SportlerService sportlerService;

	
	/**
	 * @apiNote "/sportler/{name}"
	 * @param name
	 * @return Sportler
	 * 
	 * Gibt einen Sportler zur�ck
	 */
	
	@RequestMapping("/sportler/{name}")
	public Sportler getSportlerList(@PathVariable String name){
		log.info("/sportler/{name} API Call (get)");
		return sportlerService.getSportler(name);
		
	}
	
	/**
	 * Gibt eine Liste von Sportler zur�ck
	 * @apiNote "/sportler/{name}"
	 * @param name
	 * @return Liste von Sportler
	 * 
	 * 
	 */
	
	@RequestMapping("/sportler")
	public List<Sportler> getSportlersList(){
		log.info("/sportler API Call (get)");
		return sportlerService.getSportlerList();
		
	}
	
	/**
	 * Hinzuf�gen eines Sportlers
	 * @apiNote "/sportler"
	 * @param
	 * 
	 */
	
	@RequestMapping(method=RequestMethod.POST, value="/sportler")
	public void addSportler(@RequestBody Sportler sportler) {
		log.info("/sportler API Call (post)");
		sportlerService.addSportler(sportler);
		
	}
	
	/**
	 * Update eines Sportlers
	 * @apiNote "/sportler/{name}"
	 * @param
	 * 
	 */
	
	@RequestMapping(method=RequestMethod.PUT, value="/sportler/{name}")
	public void updateSportler(@PathVariable String name,@RequestBody Sportler sportler) {
		log.info("/sportler/{name} API Call (post)");
		sportlerService.updateSportler(sportler, name);
		
	}
	
	/**
	 * L�schen eines Sportlers
	 * @apiNote "/sportler/{name}"
	 * @param
	 * 
	 */
	
	@RequestMapping(method=RequestMethod.DELETE, value="/sportler/{name}")
	public void deleteSportler(@PathVariable String name) {
		log.info("/sportler/{name} API Call (delete)");
		sportlerService.deleteSportler(name);

	}
}