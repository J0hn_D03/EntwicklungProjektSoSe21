package EntwicklungProjektSoSe21;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;


public class TrainingSessionTest {
	
	Sportler sportler = new Sportler();
	Training training = new Training();
	TrainingSession trainingsession = new TrainingSession();
	
	@Test
	@DisplayName("Pr�fen der Sportler")
	void sportlerTest(){
		
		sportler.setName("Uwe");
		sportler.setGeschlecht("M");
		sportler.setGr��e("180 cm");


		Assert.assertEquals("Uwe", sportler.getName());
		Assert.assertEquals("M", sportler.getGeschlecht());
		Assert.assertEquals("180 cm", sportler.getGr��e());

	}
	
	@Test
	@DisplayName("Pr�fen des Trainings")
	void trainingTest(){
		
		training.setTname("Tennis");
		
		
		Assert.assertEquals("Tennis", training.getTname());
		
	}

	@Test
	@DisplayName("Pr�fen der TrainingsSession")
	void trainingsessionTest(){
		
		trainingsession.setTraining(training);
		trainingsession.setSportler(sportler);
		trainingsession.setSessionnr(555);
		trainingsession.setSessionDate("01.01.2020");
		trainingsession.setSessionStart("15 Uhr");
		trainingsession.setSessionEnd("16 Uhr");
		trainingsession.setSessionGewicht("80 KG");
		
		
		Assert.assertEquals(555, trainingsession.getSessionnr());
		Assert.assertEquals("01.01.2020", trainingsession.getSessionDate());
		Assert.assertEquals("15 Uhr", trainingsession.getSessionStart());
		Assert.assertEquals("16 Uhr", trainingsession.getSessionEnd());
		Assert.assertEquals("80 KG", trainingsession.getSessionGewicht());

	}
	
	@After
	public void cleanup() {
		sportler = null;	
		training = null;
		trainingsession = null;
	
	}

}