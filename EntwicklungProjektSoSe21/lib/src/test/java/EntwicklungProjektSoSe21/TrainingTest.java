package EntwicklungProjektSoSe21;



import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;


class TrainingTest {

	Training training = new Training();
	
	
	@Test
	@DisplayName("Pr�fen des Trainingsnamens")
	void nameTest(){
		
		training.setTname("Fussball");
		Assert.assertEquals("Fussball", training.getTname());

	}
	
	@Test
	@DisplayName("Pr�fen der Beschreibung")
	void beschreibungTest(){
		
		training.setBeschreibung("Ballsport");
		Assert.assertEquals("Ballsport", training.getBeschreibung());

	}

	@After
	public void cleanup() {
		
		training = null;	
	}
	
}
