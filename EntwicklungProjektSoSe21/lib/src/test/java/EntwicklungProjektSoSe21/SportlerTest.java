package EntwicklungProjektSoSe21;

import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

class SportlerTest {
	
	Sportler sportler = new Sportler();
	
	
	@Test
	@DisplayName("Pr�fen des Namens")
	void nameTest(){
		
		sportler.setName("Klaus");
		Assert.assertEquals("Klaus", sportler.getName());

	}
	
	@Test
	@DisplayName("Pr�fen der Gr��e")
	void beschreibungTest(){
		
		sportler.setGr��e("184");
		Assert.assertEquals("184", sportler.getGr��e());

	}
	
	@Test
	@DisplayName("Pr�fen des Geschlechts")
	void geschlechtTest(){
		
		sportler.setGeschlecht("m");
		Assert.assertEquals("m", sportler.getGeschlecht());

	}

	@After
	public void cleanup() {
		
		sportler = null;	
	}

}
