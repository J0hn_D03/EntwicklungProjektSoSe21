//On Page load-registerlistenersandloadexistingvideosin datatable

$(document).ready(function() {
		calcDuration();
		loadTrainingDataTable();
		loadSportlerDataTable();
	$("#newTraining").submit(function(event) {postTraining(event);});
	$("#newSportler").submit(function(event) {postSportler(event);});
	//Load Datatable
	$('#loadtable').click(function() {
		loadTrainingDataTable();
		loadSportlerDataTable();
		
		
	}); 
});


function postSportler(event){
var training = {
	"tname" : $('input[name=trainingSessionName]').val(),
}
var trainingSession = {
	"sessionDate" : $('input[name=trainingSessionDate]').val(),
	"sessionStart" : $('input[name=trainingSessionStart]').val(),
	"sessionEnd" : $('input[name=trainingSessionEnd]').val(),
	"sessionGewicht" : $('input[name=trainingSessionGewicht]').val(),
	"training":  training
	};

var trainingsessions = [];
		trainingsessions.push(trainingSession);

if(training.tname.length !== 0){
	console.log("Es wird eine trainingSession anlegelt da Trainingsnameen : " + training.tname + " ist");
	
	var formData= {
		'name' 					: $('input[name=name]').val(),
		'beschreibung'			: $('input[name=beschreibung]').val(),
		'geschlecht' 			: $('input[name=geschlecht]').val(),
		'gr��e'					: $('input[name=gr��e]').val(),
		'trainingSession' 		: trainingsessions,
	};
}
else{ 	
	console.log("Es wurde kein Training angegeben!");
	var formData= {
	'name' 					: $('input[name=name]').val(),
	'beschreibung'			: $('input[name=beschreibung]').val(),
	'geschlecht' 			: $('input[name=geschlecht]').val(),
	'gr��e'					: $('input[name=gr��e]').val()
	};
}
$.ajax({
	
	type: 'POST', // definethetype ofHTTP verbwewanttouse(POST forourform)
	contentType: 'application/json',
	url: '/sportler', //urlwherewewanttoPOST
	data: JSON.stringify(formData), // datawewanttoPOST
	success: function( data, textStatus, jQxhr){loadSportlerDataTable();},
	error: function( jqXhr, textStatus, errorThrown){console.log(errorThrown);}}
);// stoptheform fromsubmittingthenormal wayandrefreshingthepage
event.preventDefault();}


function postTraining(event){
var formData= {
	'tname' 			: $('input[name=tname]').val(),
	'beschreibung'		: $('input[name=beschreibung]').val()
	};

$.ajax({
	
	type: 'POST', // definethetype ofHTTP verbwewanttouse(POST forourform)
	contentType: 'application/json',
	url: '/training', //urlwherewewanttoPOST
	data: JSON.stringify(formData), // datawewanttoPOST
	success: function( data, textStatus, jQxhr){loadTrainingDataTable();},
	error: function( jqXhr, textStatus, errorThrown){console.log(errorThrown);}}
);// stoptheform fromsubmittingthenormal wayandrefreshingthepage
event.preventDefault();}


function loadTrainingDataTable(){
	vartable= $('#training').DataTable({destroy: true,
		"ajax": {
			"url": "/training",//URL
			"dataSrc": ""// Causeofflat JsonObjects
				},"columns": [{ "data": "tname"},
							{ "data": "beschreibung"}
				]
			});
			
	
}

function loadSportlerDataTable(){
vartable= $('#sportler').DataTable({destroy: true,
		"ajax": {
			"url": "/sportler",//URL
			"dataSrc": ""// Causeofflat JsonObjects
				},"columns": [{ "data": "name"},
							{ "data": "geschlecht"},
							{ "data": "gr��e"},
							{ "data": "trainingSession.[ ].training.tname"},
							{ "data": "trainingSession.[ ].sessionDate"},
							{ "data": "trainingSession.[ ].sessionStart"},
							{ "data": "trainingSession.[ ].sessionEnd"},						
							{ "data": "trainingSession.[ ].sessionGewicht"}
					
				]
			});
}

function deleteSportler(){
	var delname=  $('input[name=delname]').val();
	console.log("L�sche jetzt " + delname);
	$.ajax({
    url: '/sportler' + '/' + delname,
    type: 'DELETE',
    success:  window.location.reload(),
    error: function( jqXhr, textStatus, errorThrown){console.log(errorThrown)}
    });
 };	
	
	
function deleteTraining(){
	var delname=  $('input[name=delname]').val();
	console.log("L�sche jetzt " + delname);
	$.ajax({
    url: '/training' + '/' + delname,
    type: 'DELETE',
    success:  window.location.reload(),
    error: function( jqXhr, textStatus, errorThrown){console.log(errorThrown)}
    });
   // window.location.reload();
 };	
	
	
function calcDuration(){
	let map = new Map()
 	 $.ajax({url: "/sportler", success: function(result){
   			 result.forEach(resultFunktion);
   			 map.forEach(appendToDom)
		}
	})
	
	
	function resultFunktion(item)	{
	var session = item.trainingSession;
	
	session.forEach(resultSession);
	}	

	function resultSession(item)	{
	var name = item.training.tname;
	var dauer = (item.sessionEnd - item.sessionStart);
	if (map.has(name)){
		map.set(name,  dauer + map.get(name));
	}else{
		map.set(name,  dauer);	
	}
	
	}

	
	function appendToDom(item , index)	{
		var entry = document.createElement("li"); 
		entry.innerHTML = index + " : "  + item;
		
		console.log(item , index);
		document.getElementById("listeFav").appendChild(entry);
		
	}	

};	



